---
name: The name of the blip
quadrant: Tools
ring: Hold
isNew: true
---
The multiline text here, containing the long description of a unique item, added and edited collaborively.
