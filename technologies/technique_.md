---
name: The name of the blip
quadrant: Techniques
ring: Assess
isNew: true
---

The multiline text here, containing the long description of a unique item, added and edited collaborively.
