---
name: The name of the blip
quadrant: languages-and-frameworks
ring: Adopt
isNew: true
---
The multiline text here, containing the long description of a unique item, added and edited collaborively.
